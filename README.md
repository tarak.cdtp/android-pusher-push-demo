This is a demo app for testing pusher push inside android app
To send notification from terminal or server, please follow this url
https://pusher.com/docs/beams/getting-started/android/publish-notifications

curl -H "Content-Type: application/json"      -H "Authorization: Bearer 748E69DA9C4E607B330684841672F697EE136C759BF52DF23CC022A30587192D"      -X POST "https://74b98bed-8a91-40db-b2da-d5b8176346ca.pushnotifications.pusher.com/publish_api/v1/instances/74b98bed-8a91-40db-b2da-d5b8176346ca/publishes"      -d '{"interests":["hello"],"fcm":{"notification":{"title":"Hello", "body":"Hello, world!"}}}'
